# Un environnement de développement

Pour suivre correctement les cours de développement au sein de la fabrique du numérique, nous devons installer LAMP (Linux, Apache, MySQL, PHP) afin de créer un serveur web sur nos bécanes !
Je ne vais pas m'attarder sur l'installation de LAMP, mais je vais vous redirigez vers ce site : [https://doc.ubuntu-fr.org/lamp](https://doc.ubuntu-fr.org/lamp) ou vous devrez suivre le grand **#1**

## Problème de permission

Vous avez finis d'installer LAMP, mais vous allez avoir certainement un premier probleme de permissions... Ouai c'est chiant, mais une solution existe, ouvrer votre terminal et rentrer la ligne suivante :

`chmod -R 777 /var/www`

*Cela va vous donnez TOUS les droits dans le dossier WWW et compagnie !*

## Que faire maintenant ?

C'est simple, maintenant vous allez vous rendre dans le dossier `/var/www` et rentrer dans le dossier `html`. Si vous n'avez jamais toucher au dossier il devrait y avoir une page par defaut qui se nomme `index.html`.


Vous devez comprendre qu'a partir de maintenant, tous nos dossiers, fichier de nos futurs sites, application web devrons se trouver dans les dossiers `/var/www/html` ainsi vos programmation seront accesible a l'adresse suivante [`http://localhost`](http://localhost:80).

Pour voir si cela fonctionne vous pouvez mettre les dossiers que nous avons créer la semaine dernière afin de créer une application météo. ! :) *Vous pouvez supprimer la page par défaut ! ;)*

### Comment savoir si j'ai des erreurs dans mon script ?

Bonne question ! Avant on regardais le terminal pour savoir nos erreures *(quand on utilisait la méthode PHP -S blablabla)* maintenant que nous avons notre serveur web en route h24 sur nos machines, vous allez devoir récupérer les `log` du serveur !
Rien de compliquer, rendez-vous ici : 

`/var/log/apache2/`

Et vous pouvez ouvrir le fichier `error.log` afin de regarder en temps-réel les erreurs sur votre serveur !